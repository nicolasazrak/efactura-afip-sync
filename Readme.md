# Sobre los certificados necesarios

Para poder interactuar con la afip se necesitan 2 clases de certificados.

- 1 para poder usar el protocolo SSL (cuando se usa https). Técnicamente se podría usar http, sin este certificado pero cuando se intentar usar no responde.
- 1 para encriptar los datos cuando se le envían a la afip.

Sí, es medio rendundante, pero bueno lógica de la afip.


### Sobre el certificado SSL

Este certificado permite que java se conecte por https y haga bien el handshake, esta más alla de la afip. Si se importa en el sistema de java, no debería hacer falta agregarlo, pero no esta de más tenerlo a mano.

La forma en la lo generé, fue yendo a la página y desde el firefox exportando el certificado que expone. Una vez esta ese archivo, con keytool se modifica al formato que necesita java. El proceso esta bien explicado en
> http://www.elblogdeselo.com/web-services-en-java-y-axis-solucionar-javax-net-ssl-sslhandshakeexception

para usarlo desde linux esta este otro link

> https://docs.oracle.com/cd/E19509-01/820-3503/6nf1il6er/index.html

El código es este.

`keytool -import -file C:\cascerts\firstCA.crt -alias firstCA -keystore myTrustStore.keystore`

Una vez esta eso, desde el código que se ejecute en java hay que hacer

```java
System.setProperty("javax.net.ssl.trustStore", "pathDelArchivo");
System.setProperty("javax.net.ssl.trustStorePassword", "password");
```

(aclaración: el archivo lleva password que se solicita cuando se la genera)

### Sobre el certificado para la AFIP

Este certificado es un poco más complicado. Esta documentado en la página de la afip pero es medio confuso. Pero los pasos son:

- Generar la clave privada con `openssl genrsa -out MiClavePrivada 1024`
- Generar el request para que la afip firme la clave con (llámese pedido CRC) `openssl req -new -key MiClavePrivada -subj "/C=AR/O=subj_o/CN=subj_cn/serialNumber=CUIT subj_cuit" -out MiPedidoCSR`
- Con ese archivo ir a la página de la afip y obtener la firma digital.
	- Se podría descomponer en los pasos de la página de la afip pero bueno, queda en TODO
- Con ese archivo que devuelva la afip más la clave privada generada en el primer paso con ssl se va a llevar a un archivo en el formato PKC#12. La explicación esta en https://www.palantir.com/2008/06/pkcs12/ pero de forma resumida creo que hay que ejecutar `openssl pkcs12 -export -inkey your_private_key.key  -in result.pem -name my_name -out final_result.pfx`
(esto de un solo comando lo saqué de https://www.tbs-certificates.co.uk/FAQ/en/288.html no me acuerdo como lo hice la primera vez)

### Sobre que hacer después

Al final, solamente importan los archivos generados al final de cada proceso. El de SSL es más directo. El PKC#12 hay que usarlo desde el código cuando se firma el xml y esta en el ejemplo de la afip

name := "afipSync"

version := "0.1"

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(

	"com.io7m.xom" % "xom" % "1.2.10",
	"junit" % "junit" % "4.12",
	"commons-io" % "commons-io" % "2.4",
	"xmlunit" % "xmlunit" % "1.6",
	"dom4j" % "dom4j" % "1.6.1",

	"com.google.inject" % "guice" % "4.0",
	"org.mockito" % "mockito-all" % "2.0.2-beta",


	"org.bouncycastle" % "bcprov-jdk16" % "1.46",
	"org.bouncycastle" % "bcmail-jdk16" % "1.46",
	"org.apache.axis" % "axis" % "1.4",
	"javax.xml" % "jaxrpc-api" % "1.1",
	"commons-logging" % "commons-logging" % "1.1.1",
	"commons-discovery" % "commons-discovery" % "0.4",
	"javax.activation" % "activation" % "1.1.1",
	"com.sun.mail" % "javax.mail" % "1.5.4",
	"jaxen" % "jaxen" % "1.1.6",
	"wsdl4j" % "wsdl4j" % "1.6.2"

)

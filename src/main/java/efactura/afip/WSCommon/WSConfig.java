package efactura.afip.WSCommon;


import java.io.FileInputStream;
import java.util.Properties;

public class WSConfig {

	private String p12file;
	private String p12pass;
	private String signer;
	private String dstDN;
	private Long ticketTime;
	private String endpoint;

	public WSConfig() throws Exception {

		// Read config from phile
		Properties config = new Properties();

		System.out.println("Runnging from: " + System.getProperty("user.dir"));
		config.load(new FileInputStream("../../src/test/resources/wsaa_client.properties"));

		endpoint = config.getProperty("endpoint");
		dstDN = config.getProperty("dstdn");

		p12file = config.getProperty("keystore");
		signer = config.getProperty("keystore-signer");
		p12pass = config.getProperty("keystore-password");


		// Set the keystore used by SSL
		System.setProperty("javax.net.ssl.trustStore", config.getProperty("trustStore"));
		System.setProperty("javax.net.ssl.trustStorePassword",config.getProperty("trustStore_password"));

		ticketTime = 36000L;

	}


	public String getp12file() {
		return p12file;
	}

	public String getP12pass() {
		return p12pass;
	}

	public String getSigner() {
		return signer;
	}

	public String getDstDN() {
		return dstDN;
	}

	public Long getTicketTime() {
		return ticketTime;
	}

	public String getEndpoint() {
		return endpoint;
	}


}

package efactura.afip.WSCommon;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by leaa on 20/07/2015.
 */
@XmlRootElement(name = "Err")
@XmlType(propOrder = {"code","msg"})
public class WSError {

    int code;
    String msg;

    @XmlElement(name="Code")
    public void setCode(int code) {
        this.code = code;
    }

    public WSError() {}

    @XmlElement(name="Msg")
    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    public WSError(int errorNum, String msg){
        this.code = errorNum;
        this.msg=msg;
    }

}

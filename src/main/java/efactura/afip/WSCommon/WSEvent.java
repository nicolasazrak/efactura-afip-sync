package efactura.afip.WSCommon;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by leaa on 20/07/2015.
 */
 @XmlType(propOrder = {"code","msg"})
 @XmlRootElement(name="Evt")
public class WSEvent {

    int code;
    String msg;

    @XmlElement(name="Code")
    public void setCode(int code) {
        this.code = code;
    }

    @XmlElement(name="Msg")
    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    public WSEvent() {}

    public WSEvent(int eventNum, String msg){
        this.code = eventNum;
        this.msg = msg;
    }

}

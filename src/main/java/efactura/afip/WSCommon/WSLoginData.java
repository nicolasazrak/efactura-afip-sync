package efactura.afip.WSCommon;


import java.time.LocalDateTime;

public class WSLoginData {

    private String token;
	private String sign;
	private String service;
	private LocalDateTime expiration;

    public WSLoginData(String token, String sign, LocalDateTime expiration, String service) {
        this.token = token;
        this.sign = sign;
		this.service = service;
		this.expiration = expiration;
    }

    public String getToken() {
        return token;
    }

    public String getSign() {
        return sign;
    }

	public String getService() {
		return service;
	}

	public LocalDateTime getExpiration() {
		return expiration;
	}
}

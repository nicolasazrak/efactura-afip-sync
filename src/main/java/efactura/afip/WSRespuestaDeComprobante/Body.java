package efactura.afip.WSRespuestaDeComprobante;

import javax.xml.bind.annotation.*;

/**
 * Created by leaa on 23/07/2015.
 */


@XmlRootElement(name="Body")
public class Body {
    FECAESolicitarResponse fecaeSolicitarResponse;

    public FECAESolicitarResponse getFecaeSolicitarResponse() {
        return fecaeSolicitarResponse;
    }
    @XmlElement(name="FECAESolicitarResponse")
    public void setFecaeSolicitarResponse(FECAESolicitarResponse fecaeSolicitarResponse) {
        this.fecaeSolicitarResponse = fecaeSolicitarResponse;
    }

    public Body(FECAESolicitarResponse fecaeSolicitarResponse) {
        this.fecaeSolicitarResponse = fecaeSolicitarResponse;
    }

    public Body() {
    }
}

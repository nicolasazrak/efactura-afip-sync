package efactura.afip.WSRespuestaDeComprobante;

import efactura.afip.WSCommon.WSError;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by leaa on 23/07/2015.
 */
@XmlRootElement(name="Errors")
public class Errors {
    List<WSError> errors;

    public List<WSError> getErrors() {
        return errors;
    }

    @XmlElement(name="Err")
    public void setErrors(List<WSError> errors) {
        this.errors = errors;
    }

    public Errors(List<WSError> errors) {
        this.errors = errors;
    }

    public Errors() {
    }

}

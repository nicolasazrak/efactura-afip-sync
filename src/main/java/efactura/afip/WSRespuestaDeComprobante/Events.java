package efactura.afip.WSRespuestaDeComprobante;

import efactura.afip.WSCommon.WSEvent;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by leaa on 23/07/2015.
 */
@XmlRootElement(name = "Events")
public class Events {
    List<WSEvent> events;

    public List<WSEvent> getEvents() {
        return events;
    }
    @XmlElement(name="Evt")
    public void setEvents(List<WSEvent> events) {
        this.events = events;
    }

    public Events(List<WSEvent> events) {
        this.events = events;
    }

    public Events() {
    }
}

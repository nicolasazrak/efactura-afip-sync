package efactura.afip.WSRespuestaDeComprobante;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by leaa on 23/07/2015.
 */
@XmlRootElement(name="FECAESolicitarResponse")
public class FECAESolicitarResponse {

    FECAESolicitarResult fecaeSolicitarResult;

    @XmlElement(name="FECAESolicitarResult")
    public void setFecaeSolicitarResult(FECAESolicitarResult fecaeSolicitarResult) {
        this.fecaeSolicitarResult = fecaeSolicitarResult;
    }
    public FECAESolicitarResult getFecaeSolicitarResult() {
        return fecaeSolicitarResult;
    }

    public FECAESolicitarResponse(FECAESolicitarResult fecaeSolicitarResult) {
        this.fecaeSolicitarResult = fecaeSolicitarResult;
    }

	public FECAESolicitarResponse(){}

}

package efactura.afip.WSRespuestaDeComprobante;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by leaa on 23/07/2015.
 */
@XmlRootElement(name= "FECAESolicitarResult")
@XmlType(propOrder = {"feCabResp", "feDetResp", "events", "errors"})
public class FECAESolicitarResult {

    FeCabResp feCabResp;
    FeDetResp feDetResp;
    Events events;
    Errors errors;

    public FeCabResp getFeCabResp() {
        return feCabResp;
    }
    @XmlElement(name="FeCabResp")
    public void setFeCabResp(FeCabResp feCabResp) {
        this.feCabResp = feCabResp;
    }

    public FeDetResp getFeDetResp() {
        return feDetResp;
    }
    @XmlElement(name="FeDetResp")
    public void setFeDetResp(FeDetResp feDetResp) {
        this.feDetResp = feDetResp;
    }

    public Events getEvents() {
        return events;
    }
    @XmlElement(name="Events")
    public void setEvents(Events events) {
        this.events = events;
    }

    public Errors getErrors() {
        return errors;
    }
    @XmlElement(name="Errors")
    public void setErrors(Errors errors) {
        this.errors = errors;
    }

    public FECAESolicitarResult() {
    }

    public FECAESolicitarResult(FeCabResp feCabResp, FeDetResp feDetResp, Events events, Errors errors) {

        this.feCabResp = feCabResp;
        this.feDetResp = feDetResp;
        this.events = events;
        this.errors = errors;
    }
}

package efactura.afip.WSRespuestaDeComprobante;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by leaa on 20/07/2015.
 */
@XmlRootElement(name = "FeCabResp")
@XmlType(propOrder = {"cuit","ptoVta","cbteTipo","fchProceso","cantReg","resultado"})
public class FeCabResp {

    long Cuit;
    int PtoVta;
    int CbteTipo;
    String FchProceso;
    int CantReg;
    String Resultado;

    public FeCabResp() {}

    @XmlElement(name= "Cuit")
    public void setCuit(long cuit) {
        this.Cuit = cuit;
    }
	public long getCuit() {
		return Cuit;
	}

    @XmlElement(name= "PtoVta")
    public void setPtoVta(int ptoVta) {
        PtoVta = ptoVta;
    }
	public int getPtoVta() {
		return PtoVta;
	}

    @XmlElement(name= "CbteTipo")
    public void setCbteTipo(int cbteTipo) {
        CbteTipo = cbteTipo;
    }
	public int getCbteTipo() {
		return CbteTipo;
	}

    @XmlElement(name= "FchProceso")
    public void setFchProceso(String fchProceso) {
        FchProceso = fchProceso;
    }
	public String getFchProceso() {
		return FchProceso;
	}

    @XmlElement(name= "CantReg")
    public void setCantReg(int cantReg) {
        CantReg = cantReg;
    }
	public int getCantReg() {
		return CantReg;
	}

    @XmlElement(name= "Resultado")
    public void setResultado(String resultado) {
        Resultado = resultado;
    }
	public String getResultado() {
		return Resultado;
	}

    public FeCabResp(long cuit, int ptoVta, int cbteTipo, String fchProceso, int cantReg, String resultado) {
        Cuit = cuit;
        PtoVta = ptoVta;
        CbteTipo = cbteTipo;
        FchProceso = fchProceso;
        CantReg = cantReg;
        Resultado = resultado;
    }

}

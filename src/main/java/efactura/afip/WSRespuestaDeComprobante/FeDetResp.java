package efactura.afip.WSRespuestaDeComprobante;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by leaa on 23/07/2015.
 */
@XmlRootElement(name = "FeDetResp")
public class FeDetResp {
    FeDetResponse FEDetResponse;

    public FeDetResponse getFEDetResponse() {
        return FEDetResponse;
    }
    @XmlElement(name="FECAEDetResponse")
    public void setFEDetResponse(FeDetResponse FEDetResponse) {
        this.FEDetResponse = FEDetResponse;
    }

    public FeDetResp() {
    }

    public FeDetResp(FeDetResponse feDetResponse) {

        this.FEDetResponse = feDetResponse;
    }
}

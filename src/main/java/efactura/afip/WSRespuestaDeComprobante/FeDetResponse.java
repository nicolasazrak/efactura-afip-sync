package efactura.afip.WSRespuestaDeComprobante;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by leaa on 20/07/2015.
 */
@XmlRootElement(name="FECAEDetResponse")
@XmlType(propOrder = {"concepto","docTipo","docNro","cbteDesde","cbteHasta","resultado","CAE","cbteFch","CAEFchVto","observaciones"})
public class FeDetResponse {
    //Inicio FeDetResponse
    int Concepto;
    int DocTipo;
    long DocNro;
    long CbteDesde;
    long CbteHasta;
    String Resultado;
    String CAE;
    String CbteFch;
    String CAEFchVto;
    Observaciones observaciones;

    public int getConcepto() {
        return Concepto;
    }
    @XmlElement(name= "Concepto")
    public void setConcepto(int concepto) {
        Concepto = concepto;
    }

    public int getDocTipo() {
        return DocTipo;
    }
    @XmlElement(name= "DocTipo")
    public void setDocTipo(int docTipo) {
        DocTipo = docTipo;
    }

    public long getDocNro() {
        return DocNro;
    }
    @XmlElement(name= "DocNro")
    public void setDocNro(long docNro) {
        DocNro = docNro;
    }

    public long getCbteDesde() {
        return CbteDesde;
    }
    @XmlElement(name= "CbteDesde")
    public void setCbteDesde(long cbteDesde) {
        CbteDesde = cbteDesde;
    }

    public long getCbteHasta() {
        return CbteHasta;
    }
    @XmlElement(name= "CbteHasta")
    public void setCbteHasta(long cbteHasta) {
        CbteHasta = cbteHasta;
    }

    public String getResultado() {
        return Resultado;
    }
    @XmlElement(name= "Resultado")
    public void setResultado(String resultado) {
        Resultado = resultado;
    }

    public String getCAE() {
        return CAE;
    }
    @XmlElement(name= "CAE")
    public void setCAE(String CAE) {
        this.CAE = CAE;
    }

    public String getCbteFch() {
        return CbteFch;
    }
    @XmlElement(name= "CbteFch")
    public void setCbteFch(String cbteFch) {
        CbteFch = cbteFch;
    }

    public String getCAEFchVto() {
        return CAEFchVto;
    }
    @XmlElement(name= "CAEFchVto")
    public void setCAEFchVto(String CAEFchVto) {
        this.CAEFchVto = CAEFchVto;
    }

    public Observaciones getObservaciones() {
        return observaciones;
    }
    @XmlElement(name= "Observaciones")
    public void setObservaciones(Observaciones observaciones) {
        this.observaciones = observaciones;
    }

    public FeDetResponse() {
    }

    public FeDetResponse(int concepto, int docTipo, long docnro, long cbteDesde, long cbteHasta, String resultado, String CAE, String cbteFch, String CAEFchVto, Observaciones observaciones) {

        Concepto = concepto;
        DocTipo = docTipo;
        DocNro = docnro;
        CbteDesde = cbteDesde;
        CbteHasta = cbteHasta;
        Resultado = resultado;
        this.CAE = CAE;
        CbteFch = cbteFch;
        this.CAEFchVto = CAEFchVto;
        this.observaciones = observaciones;
    }
}

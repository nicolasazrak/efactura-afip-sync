package efactura.afip.WSRespuestaDeComprobante;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by leaa on 20/07/2015.
 */
@XmlRootElement(name="Obs")
@XmlType(propOrder = {"code","msg"})
public class Observacion {
    int Code;
    String Msg;

    public Observacion() {
    }

    public int getCode() {
        return Code;
    }
    @XmlElement(name="Code")
    public void setCode(int code) {
        Code = code;
    }

    public String getMsg() {
        return Msg;
    }
    @XmlElement(name="Msg")
    public void setMsg(String msg) {
        Msg = msg;
    }

    public Observacion(int code, String msg) {
        Code = code;
        Msg = msg;
    }
}

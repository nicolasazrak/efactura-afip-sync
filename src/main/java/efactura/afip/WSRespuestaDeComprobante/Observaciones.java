package efactura.afip.WSRespuestaDeComprobante;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by leaa on 23/07/2015.
 */
@XmlRootElement(name="Observaciones")
public class Observaciones {

    List<Observacion> observacion;

    public Observaciones() {
    }

    public Observaciones(List<Observacion> observacion) {

        this.observacion = observacion;
    }

    public List<Observacion> getObservacion() {
        return observacion;
    }
    @XmlElement(name="Obs")
    public void setObservacion(List<Observacion> observacion) {
        this.observacion = observacion;
    }

}

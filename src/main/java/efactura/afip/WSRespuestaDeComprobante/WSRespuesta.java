package efactura.afip.WSRespuestaDeComprobante;

import org.apache.commons.io.FileUtils;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.File;
import java.io.IOException;

/**
 * Created by leaa on 23/07/2015.
 */
@XmlType(propOrder = {"header","body"})
@XmlRootElement(name = "Envelope", namespace = "http://schemas.xmlsoap.org/soap/envelope/")
public class WSRespuesta {
    String header="";//no tiene nada segun el WS 2.6
    Body body;

    public Body getBody() {
        return body;
    }

    public String getHeader() {
        return header;
    }

    @XmlElement(name = "Header",required = true, namespace = "http://schemas.xmlsoap.org/soap/envelope/")
    public void setHeader(String Header){
        this.header =Header;

    }

    public WSRespuesta(Body body) {
        this.body = body;
    }

    @XmlElement(name= "Body", required=true, namespace = "http://schemas.xmlsoap.org/soap/envelope/")
    public void setBody(Body body){
        this.body = body;
    }

    public WSRespuesta() {
    }

    public static WSRespuesta createWsRespuesta(String xmlString) throws JAXBException, IOException {
        FileUtils.writeStringToFile(new File("respuesta"),xmlString);
        File xml = new File("respuesta");

        JAXBContext jaxbContext = JAXBContext.newInstance(WSRespuesta.class);

        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        WSRespuesta wsR = (WSRespuesta)jaxbUnmarshaller.unmarshal( xml );
        return wsR;

    }

    public static WSRespuesta createWsRespuesta(File xml) throws JAXBException, IOException {


        JAXBContext jaxbContext = JAXBContext.newInstance(WSRespuesta.class);

        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        WSRespuesta wsR = (WSRespuesta)jaxbUnmarshaller.unmarshal( xml );
        return wsR;

    }

}

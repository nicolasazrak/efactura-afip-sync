/**
 * Created by leaa on 23/07/2015.
 */
@XmlSchema(
        xmlns = {
                @XmlNs(prefix = "soap",
                        namespaceURI = "http://www.w3.org/2003/05/soap-envelope"),

                @XmlNs(prefix = "ar",
                        namespaceURI = "http://ar.gov.afip.dif.fev1/")})
package efactura.afip.WSRespuestaDeComprobante;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlSchema;
package efactura.afip.models.Factura;


public class Cabecera {

	public Cabecera setCuit(long cuit) {
		this.cuit = cuit;
		return this;
	}

	public Cabecera setPuntoDeVenta(int puntoDeVenta) {
		this.puntoDeVenta = puntoDeVenta;
		return this;
	}

	public Cabecera setFecha(String fecha) {
		this.fecha = fecha;
		return this;
	}

	public Cabecera setTipo(int tipo) {
		this.tipo = tipo;
		return this;
	}

	public Cabecera setConcepto(int concepto) {
		this.concepto = concepto;
		return this;
	}

	public Cabecera setDocumentoTipo(int documentoTipo) {
		this.documentoTipo = documentoTipo;
		return this;
	}

	public Cabecera setDesde(long desde) {
		this.desde = desde;
		return this;
	}

	public Cabecera setNumero(long numero) {
		this.numero = numero;
		return this;
	}

	public Cabecera setHasta(long hasta) {
		this.hasta = hasta;
		return this;
	}

	private long cuit;
	private int puntoDeVenta;
	private String fecha;
	private int tipo;
	private int concepto;
	private int documentoTipo;
	private long numero;
	private long desde;
	private long hasta;

	public Cabecera(){}


	public long getCuit() {
		return cuit;
	}

	public int getPuntoDeVenta() {
		return puntoDeVenta;
	}

	public String getFecha() {
		return fecha;
	}

	public int getTipo() {
		return tipo;
	}

	public int getConcepto() {
		return concepto;
	}

	public int getDocumentoTipo() {
		return documentoTipo;
	}

	public long getNumero() {
		return numero;
	}

	public long getDesde() {
		return desde;
	}

	public long getHasta() {
		return hasta;
	}


}

package efactura.afip.models.Factura;

import efactura.afip.WSCommon.WSLoginData;
import nu.xom.Document;
import nu.xom.Element;

import java.util.ArrayList;
import java.util.List;

public class Factura {

	//Inicio Auth
	String token;
	String sign;
	long cuit;
	//fin Auth

	//Inicio FeCAEReq

	//Inicio FeCabReq
	int cantReg;
	int ptoVta;
	int cbteTipo;
	//fin FeCabReq

	//Inicio FeDetReq

	//Inicio FECAEDetRequest
	int concepto;
	int doctTipo;
	long docNro;
	long cbteDesde;
	long cbteHasta;
	String cbteFch;
	double impTotal;
	double impTotConc;
	double impNeto;
	double impOpEx;
	double impTrib;
	double impIVA;
	String fchServDesde;
	String fchServHasta;
	String fchVtoPago;
	String monId;
	double monCotiz;

	//Inicio CbtesAsoc OPCIONAL
	List<ComprobanteAsociado> cbtesAsoc;
	//Fin CbtesAsoc

	//Inicio Tributos OPCIONAL
	List<Tributo> tributos = new ArrayList<Tributo>();
	//Fin Tributos

	//Inicio IVA OPCIONAL
	List<IVA> listaIva = new ArrayList<IVA>();
	//Fin IVA

	//Fin FECAEDetRequest
	//Fin FeDetReq
	//Fin FeCAEReq

	public String generarXML() {

		/*Faltan IVA, Tributos y Cbtes asociados porque no se si son necesarios*/
		//encabezado
		//String soapNamespace = "http://www.w3.org/2003/05/soapenvelope";
		String soapNamespace = "http://schemas.xmlsoap.org/soap/envelope/";
		Element root = new Element("Envelope", soapNamespace);
		//root.addNamespaceDeclaration("ar", "http://ar.gov.afip.dif.FEV1/");
		root.addNamespaceDeclaration("xsi", "http://www.w3.org/2001/XMLSchema-instance");
		root.addNamespaceDeclaration("xsd", "http://www.w3.org/2001/XMLSchema");


		//header
		root.appendChild(new Element("Header", soapNamespace));
		//body
		Element body = new Element("Body", soapNamespace);
		root.appendChild(body);
		//FECAESolicitar
		String fev1Namespace = "http://ar.gov.afip.dif.FEV1/";
		Element FECAESolicitar = new Element("FECAESolicitar", fev1Namespace);
		//FECAESolicitar.addNamespaceDeclaration("fev","http://ar.gov.afip.dif.FEV1/");
		body.appendChild(FECAESolicitar);
		//Auth
		Element Auth = new Element("Auth", fev1Namespace);

		FECAESolicitar.appendChild(Auth);
		Element Token = new Element("Token", fev1Namespace);
		Token.appendChild(token);
		Auth.appendChild(Token);
		Element Sign = new Element("Sign", fev1Namespace);
		Sign.appendChild(sign);
		Auth.appendChild(Sign);
		Element Cuit = new Element("Cuit", fev1Namespace);
		Cuit.appendChild(String.valueOf(cuit));
		Auth.appendChild(Cuit);
		//FeCAEReq
		Element FeCAEReq = new Element("FeCAEReq", fev1Namespace);
		FECAESolicitar.appendChild(FeCAEReq);
		//FeCabReq
		Element FeCabReq = new Element("FeCabReq", fev1Namespace);
		FeCAEReq.appendChild(FeCabReq);
		Element CantReg = new Element("CantReg", fev1Namespace);
		CantReg.appendChild(String.valueOf(cantReg));
		Element PtoVta = new Element("PtoVta", fev1Namespace);
		PtoVta.appendChild(String.valueOf(ptoVta));
		Element CbteTipo = new Element("CbteTipo", fev1Namespace);
		CbteTipo.appendChild(String.valueOf(cbteTipo));
		FeCabReq.appendChild(CantReg);
		FeCabReq.appendChild(PtoVta);
		FeCabReq.appendChild(CbteTipo);
		//FeDetReq
		Element FeDetReq = new Element("FeDetReq", fev1Namespace);
		FeCAEReq.appendChild(FeDetReq);
		Element FECAEDetRequest = new Element("FECAEDetRequest", fev1Namespace);
		FeDetReq.appendChild(FECAEDetRequest);
		Element Concepto = new Element("Concepto", fev1Namespace);
		Element DocTipo = new Element("DocTipo", fev1Namespace);
		Element DocNro = new Element("DocNro", fev1Namespace);
		Element CbteDesde = new Element("CbteDesde", fev1Namespace);
		Element CbteHasta = new Element("CbteHasta", fev1Namespace);
		Element CbteFch = new Element("CbteFch", fev1Namespace);
		Element ImpTotal = new Element("ImpTotal", fev1Namespace);
		Element ImpTotConc = new Element("ImpTotConc", fev1Namespace);
		Element ImpNeto = new Element("ImpNeto", fev1Namespace);
		Element ImpOpEx = new Element("ImpOpEx", fev1Namespace);
		Element ImpTrib = new Element("ImpTrib", fev1Namespace);
		Element ImpIVA = new Element("ImpIVA", fev1Namespace);
		Element FchServDesde = new Element("FchServDesde", fev1Namespace);
		Element FchServHasta = new Element("FchServHasta", fev1Namespace);
		Element FchVtoPago = new Element("FchVtoPago", fev1Namespace);
		Element MonId = new Element("MonId", fev1Namespace);
		Element MonCotiz = new Element("MonCotiz", fev1Namespace);
		Concepto.appendChild(String.valueOf(concepto));
		DocTipo.appendChild(String.valueOf(doctTipo));
		DocNro.appendChild(String.valueOf(docNro));
		CbteDesde.appendChild(String.valueOf(cbteDesde));
		CbteHasta.appendChild(String.valueOf(cbteHasta));
		CbteFch.appendChild(cbteFch);
		ImpTotal.appendChild(String.valueOf(impTotal));
		ImpTotConc.appendChild(String.valueOf(impTotConc));
		ImpNeto.appendChild(String.valueOf(impNeto));
		ImpOpEx.appendChild(String.valueOf(impOpEx));
		ImpTrib.appendChild(String.valueOf(impTrib));
		ImpIVA.appendChild(String.valueOf(impIVA));
		FchServDesde.appendChild(fchServDesde);
		FchServHasta.appendChild(fchServHasta);
		FchVtoPago.appendChild(fchVtoPago);
		MonId.appendChild(monId);
		MonCotiz.appendChild(String.valueOf(monCotiz));
		FECAEDetRequest.appendChild(Concepto);
		FECAEDetRequest.appendChild(DocTipo);
		FECAEDetRequest.appendChild(DocNro);
		FECAEDetRequest.appendChild(CbteDesde);
		FECAEDetRequest.appendChild(CbteHasta);
		FECAEDetRequest.appendChild(CbteFch);
		FECAEDetRequest.appendChild(ImpTotal);
		FECAEDetRequest.appendChild(ImpTotConc);
		FECAEDetRequest.appendChild(ImpNeto);
		FECAEDetRequest.appendChild(ImpOpEx);
		FECAEDetRequest.appendChild(ImpTrib);
		FECAEDetRequest.appendChild(ImpIVA);
		FECAEDetRequest.appendChild(FchServDesde);
		FECAEDetRequest.appendChild(FchServHasta);
		FECAEDetRequest.appendChild(FchVtoPago);
		FECAEDetRequest.appendChild(MonId);
		FECAEDetRequest.appendChild(MonCotiz);
		if (tributos.size() > 0) {
			Element Tributos = new Element("Tributos", fev1Namespace);
			FECAEDetRequest.appendChild(Tributos);
			for (int i = 0; i < tributos.size(); i++) {
				Element Tributo = new Element("Tributo", fev1Namespace);
				Element Id = new Element("Id", fev1Namespace);
				Element Desc = new Element("Desc", fev1Namespace);
				Element BaseImp = new Element("BaseImp", fev1Namespace);
				Element Alic = new Element("Alic", fev1Namespace);
				Element Importe = new Element("Importe", fev1Namespace);
				Id.appendChild(String.valueOf(tributos.get(i).id));
				Desc.appendChild(String.valueOf(tributos.get(i).Desc));
				BaseImp.appendChild(String.valueOf(tributos.get(i).BaseImp));
				Alic.appendChild(String.valueOf(tributos.get(i).Alic));
				Importe.appendChild(String.valueOf(tributos.get(i).Importe));
				Tributo.appendChild(Id);
				Tributo.appendChild(Desc);
				Tributo.appendChild(BaseImp);
				Tributo.appendChild(Alic);
				Tributo.appendChild(Importe);
				Tributos.appendChild(Tributo);

			}
		}

		if (listaIva.size() > 0) {
			Element Iva = new Element("Iva", fev1Namespace);
			FECAEDetRequest.appendChild(Iva);
			for (int i = 0; i < listaIva.size(); i++) {
				Element AlicIva = new Element("AlicIva", fev1Namespace);
				Element Id = new Element("Id", fev1Namespace);
				Element BaseImp = new Element("BaseImp", fev1Namespace);
				Element Importe = new Element("Importe", fev1Namespace);
				Id.appendChild(String.valueOf(listaIva.get(i).id));
				BaseImp.appendChild(String.valueOf(listaIva.get(i).BaseImp));
				Importe.appendChild(String.valueOf(listaIva.get(i).Importe));
				AlicIva.appendChild(Id);
				AlicIva.appendChild(BaseImp);
				AlicIva.appendChild(Importe);
				Iva.appendChild(AlicIva);

			}


		}


		Document doc = new Document(root);


		//TODO:Esto es para testeo
		/*try {
			Serializer serializer = new Serializer(System.out, "utf-8");
			serializer.setIndent(4);
			serializer.setMaxLength(64);
			serializer.write(doc);
		} catch (IOException ex) {
			System.err.println(ex);
		}*/

		return doc.toXML();


	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public long getCuit() {
		return cuit;
	}

	public void setCuit(long cuit) {
		this.cuit = cuit;
	}

	public int getCantReg() {
		return cantReg;
	}

	public void setCantReg(int cantReg) {
		this.cantReg = cantReg;
	}

	public int getPtoVta() {
		return ptoVta;
	}

	public void setPtoVta(int ptoVta) {
		this.ptoVta = ptoVta;
	}

	public int getCbteTipo() {
		return cbteTipo;
	}

	public void setCbteTipo(int cbteTipo) {
		this.cbteTipo = cbteTipo;
	}

	public int getConcepto() {
		return concepto;
	}

	public void setConcepto(int concepto) {
		this.concepto = concepto;
	}

	public int getDoctTipo() {
		return doctTipo;
	}

	public void setDoctTipo(int doctTipo) {
		this.doctTipo = doctTipo;
	}

	public long getDocNro() {
		return docNro;
	}

	public void setDocNro(long docNro) {
		this.docNro = docNro;
	}

	public long getCbteDesde() {
		return cbteDesde;
	}

	public void setCbteDesde(long cbteDesde) {
		this.cbteDesde = cbteDesde;
	}

	public long getCbteHasta() {
		return cbteHasta;
	}

	public void setCbteHasta(long cbteHasta) {
		this.cbteHasta = cbteHasta;
	}

	public String getCbteFch() {
		return cbteFch;
	}

	public void setCbteFch(String cbteFch) {
		this.cbteFch = cbteFch;
	}

	public double getImpTotal() {
		return impTotal;
	}

	public void setImpTotal(double impTotal) {
		this.impTotal = impTotal;
	}

	public double getImpTotConc() {
		return impTotConc;
	}

	public void setImpTotConc(double impTotConc) {
		this.impTotConc = impTotConc;
	}

	public double getImpNeto() {
		return impNeto;
	}

	public void setImpNeto(double impNeto) {
		this.impNeto = impNeto;
	}

	public double getImpOpEx() {
		return impOpEx;
	}

	public void setImpOpEx(double impOpEx) {
		this.impOpEx = impOpEx;
	}

	public double getImpTrib() {
		return impTrib;
	}

	public void setImpTrib(double impTrib) {
		this.impTrib = impTrib;
	}

	public double getImpIVA() {
		return impIVA;
	}

	public void setImpIVA(double impIVA) {
		this.impIVA = impIVA;
	}

	public String getFchServDesde() {
		return fchServDesde;
	}

	public void setFchServDesde(String fchServDesde) {
		this.fchServDesde = fchServDesde;
	}

	public String getFchServHasta() {
		return fchServHasta;
	}

	public void setFchServHasta(String fchServHasta) {
		this.fchServHasta = fchServHasta;
	}

	public String getFchVtoPago() {
		return fchVtoPago;
	}

	public void setFchVtoPago(String fchVtoPago) {
		this.fchVtoPago = fchVtoPago;
	}

	public String getMonId() {
		return monId;
	}

	public void setMonId(String monId) {
		this.monId = monId;
	}

	public double getMonCotiz() {
		return monCotiz;
	}

	public void setMonCotiz(double monCotiz) {
		this.monCotiz = monCotiz;
	}

	public List<ComprobanteAsociado> getCbtesAsoc() {
		return cbtesAsoc;
	}

	public void setCbtesAsoc(List<ComprobanteAsociado> cbtesAsoc) {
		this.cbtesAsoc = cbtesAsoc;
	}

	public List<Tributo> getTributos() {
		return tributos;
	}

	public void setTributos(List<Tributo> tributos) {
		this.tributos = tributos;
	}

	public List<IVA> getListaIva() {
		return listaIva;
	}

	public void setListaIva(List<IVA> listaIva) {
		this.listaIva = listaIva;
	}

	//constructor parcial, no incluye los datos opcionales
	public Factura(Cabecera cabecera, int cantReg, double impTotal, double impTotConc, double impNeto, double impOpEx, double impTrib, double impIVA, String fchServDesde, String fchServHasta, String fchVtoPago, Moneda moneda) {

		this.cuit = cabecera.getCuit();
		this.ptoVta = cabecera.getPuntoDeVenta();
		this.cbteTipo = cabecera.getTipo();
		this.concepto = cabecera.getConcepto();
		this.doctTipo = cabecera.getDocumentoTipo();
		this.docNro = cabecera.getNumero();
		this.cbteDesde = cabecera.getDesde();
		this.cbteHasta = cabecera.getHasta();
		this.cbteFch = cabecera.getFecha();

		this.cantReg = cantReg;
		this.impTotal = impTotal;
		this.impTotConc = impTotConc;
		this.impNeto = impNeto;
		this.impOpEx = impOpEx;
		this.impTrib = impTrib;
		this.impIVA = impIVA;
		this.fchServDesde = fchServDesde;
		this.fchServHasta = fchServHasta;
		this.fchVtoPago = fchVtoPago;

		this.monId = moneda.getId();
		this.monCotiz = moneda.getCotizacion();
	}

	public void setLoginData(WSLoginData loginData){
		this.token = loginData.getToken();
		this.sign = loginData.getSign();
	}

	public Factura() {}

	public void agregarIva(short id, double BaseImp, double Importe) {
		listaIva.add(new IVA(id, BaseImp, Importe));
	}

	public void agregarTributo(short id, String Desc, double BaseImp, double Alic, double Importe) {
		tributos.add(new Tributo(id, Desc, BaseImp, Alic, Importe));
	}

}

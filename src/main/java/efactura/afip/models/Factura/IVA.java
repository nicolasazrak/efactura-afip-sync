package efactura.afip.models.Factura;


public class IVA {

    short id;
    double BaseImp;
    double Importe;

    public short getId() {
        return id;
    }

    public double getBaseImp() {
        return BaseImp;
    }

    public IVA(short id, double baseImp, double importe) {
        this.id = id;
        BaseImp = baseImp;
        Importe = importe;
    }

    public double getImporte() {
        return Importe;
    }

}

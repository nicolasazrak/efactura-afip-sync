package efactura.afip.models.Factura;


public class Moneda {

	private String id;
	private double cotizacion;

	public Moneda(String id, double cotizacion){
		this.id = id;
		this.cotizacion = cotizacion;
	}

	public static Moneda Pesos(){
		return new Moneda("PES", 1);
	}


	public double getCotizacion() {
		return cotizacion;
	}

	public String getId() {
		return id;
	}
}

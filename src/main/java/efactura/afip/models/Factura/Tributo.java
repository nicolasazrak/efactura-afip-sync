package efactura.afip.models.Factura;

public class Tributo {

    short id;
    String Desc;
    double BaseImp;
    double Alic;
    double Importe;

    public short getId() {
        return id;
    }

    public String getDesc() {
        return Desc;
    }

    public double getBaseImp() {
        return BaseImp;
    }

    public double getAlic() {
        return Alic;
    }

    public double getImporte() {
        return Importe;
    }

    public Tributo(short id, String desc, double baseImp, double alic, double importe) {
        this.id = id;
        Desc = desc;
        BaseImp = baseImp;
        Alic = alic;
        Importe = importe;
    }

}

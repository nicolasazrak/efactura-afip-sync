package efactura.afip.operations;


import com.google.inject.AbstractModule;
import com.google.inject.Provides;

import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPException;

public class AfipWebServicesBindings extends AbstractModule {

	@Override
	protected void configure() {}

	@Provides
	SOAPConnectionFactory provideSOAPConnectionFactory() throws SOAPException {
		return SOAPConnectionFactory.newInstance();
	}

}

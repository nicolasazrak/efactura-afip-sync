package efactura.afip.operations;

import com.google.inject.Inject;
import efactura.afip.WSCommon.WSConfig;
import efactura.afip.WSCommon.WSLoginData;
import efactura.afip.WSRespuestaDeComprobante.WSRespuesta;
import efactura.afip.models.Factura.Factura;
import efactura.afip.utils.Reference;
import efactura.afip.utils.xmlStringParser;

import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import java.net.URL;


public class WSSolicitarCAE {

	protected SOAPConnection connection;
	protected WSSolicitudTA wsSolicitudTA;
	protected WSUltimoComprobante wSUltimoComprobante;

	@Inject
	private WSSolicitarCAE(SOAPConnectionFactory SOAPConnection, WSSolicitudTA wsSolicitudTA, WSUltimoComprobante wSUltimoComprobante) throws SOAPException {
		this.connection = SOAPConnection.createConnection();
		this.wsSolicitudTA = wsSolicitudTA;
		this.wSUltimoComprobante = wSUltimoComprobante;
	}



	public String solicitar(WSConfig config, WSLoginData loginData, Factura solicitud) throws Exception {


		/*
			WsSolicitudComprobante solicitador = new WsSolicitudComprobante("a","b",1,2,3,4,5,6,7,8,9,"c",0.1,0.2,0.3,0.4,0.5,0.6,"d","e","f","g",0.7);
			solicitador.generarXML();
			FeCabResp feCabResp= new FeCabResp(123,10,15,"10/5/06",15,"sad");
			List<Observacion> observs = new ArrayList<Observacion>();
			observs.add(new Observacion(855,"dfgfgdfgdfgd fdgdfgdfg"));
			Observaciones observaciones = new Observaciones(observs);
			FeDetResponse feDetResponse = new FeDetResponse(85,9657148,549841,541981,4857,"asdasdaw","148597521","10/9/06","sgfdfgtring",observaciones);
			FeDetResp feDetResp=new FeDetResp(feDetResponse);
			List<WSEvent> evs = new ArrayList<WSEvent>();
			evs.add(new WSEvent(789156456,"dsfgdfger"));
			Events events = new Events(evs);
			List<WSError> errs = new ArrayList<WSError>();
			errs.add(new WSError(87715,"fsdgsdfgs"));
			Errors errors = new Errors(errs);
			FECAESolicitarResult fECAESolicitarResult= new FECAESolicitarResult(feCabResp, feDetResp,events,errors);
			FECAESolicitarResponse fecaeSolicitarResponse=new FECAESolicitarResponse(fECAESolicitarResult);
			Body body = new Body(fecaeSolicitarResponse);
			WSRespuesta wsRespuesta = new WSRespuesta(body);
			JAXBContext jaxbContext = JAXBContext.newInstance( WSRespuesta.class );
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(wsRespuesta, System.out );
			File test2 = new File("src/main/testFiles/test2.xml");
			new WSRespuesta(test2);
			PrivateKey privKey = efactura.afip.keyReader.privateKeyRead(efactura.afip.utils.Reference.rutaPrivateKeyTest);
			WSLoginData loginData = efactura.afip.Operations.WSSolicitudTA.solicitarAuth(null, null, "wsfe", 10000, privKey);
		*/

		solicitud.setLoginData(loginData);

		//se asigna la url
		URL endpoint = new URL(Reference.wsCaeUrl);


		SOAPMessage message = xmlStringParser.getSoapMessageFromString(solicitud.generarXML());
		message.getMimeHeaders().addHeader("SOAPAction", "http://ar.gov.afip.dif.FEV1/FECAESolicitar");
		message.setProperty(SOAPMessage.WRITE_XML_DECLARATION, "true");
		message.setProperty(SOAPMessage.CHARACTER_SET_ENCODING, "UTF-8");

		System.out.println(xmlStringParser.getStringFromSoapMessage(message));

		SOAPMessage response = connection.call(message, endpoint);

		String xmlResponse = xmlStringParser.getStringFromSoapMessage(response);

		/* TODO chequear errores */

		System.out.println(xmlResponse + "\n");
		WSRespuesta respuesta = WSRespuesta.createWsRespuesta(xmlResponse);

		System.out.println("EL PUTO CAE ES: " + respuesta.getBody().getFecaeSolicitarResponse().getFecaeSolicitarResult().getFeDetResp().getFEDetResponse().getCAE());

		return respuesta.getBody().getFecaeSolicitarResponse().getFecaeSolicitarResult().getFeDetResp().getFEDetResponse().getCAE();

	}

}

package efactura.afip.operations;

import com.google.inject.Inject;
import com.sun.org.apache.xerces.internal.jaxp.datatype.XMLGregorianCalendarImpl;
import efactura.afip.WSCommon.WSConfig;
import efactura.afip.WSCommon.WSLoginData;
import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.apache.axis.encoding.Base64;
import org.apache.axis.encoding.XMLType;
import org.bouncycastle.cms.CMSProcessable;
import org.bouncycastle.cms.CMSProcessableByteArray;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.CMSSignedDataGenerator;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.dom4j.Document;
import org.dom4j.io.SAXReader;

import javax.xml.rpc.ParameterMode;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.net.URL;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Security;
import java.security.cert.CertStore;
import java.security.cert.CollectionCertStoreParameters;
import java.security.cert.X509Certificate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;


public class WSSolicitudTA {

	/**
	 * ApacheAxis Client, makes the request
	 */
	private Service service;

	@Inject
	private WSSolicitudTA(Service service){
		this.service = service;
	}


	protected String invokeWsaa(byte[] LoginTicketRequest_xml_cms, String endpoint) throws Exception {

        Call call = (Call) service.createCall();

        //
        // Prepare the call for the Web service
        //
        call.setTargetEndpointAddress(new URL(endpoint));
        call.setOperationName("loginCms");
        call.addParameter("request", XMLType.XSD_STRING, ParameterMode.IN);
        call.setReturnType(XMLType.XSD_STRING);

        //
        // Make the actual call and assign the answer to a String
        //
        return ((String) call.invoke(new Object [] {
        	Base64.encode(LoginTicketRequest_xml_cms)
        }));

    }

    //
    // Create the CMS Message
    //
    protected byte [] createCms(String p12file, String p12pass, String signer, String dstDN,
															String service, Long TicketTime) throws Exception {


        //
        // Manage Keys & Certificates
        //

        // Create a keystore using keys from the pkcs#12 p12file
        KeyStore ks = KeyStore.getInstance("pkcs12");

		InputStream p12stream = new FileInputStream(p12file) ;
        ks.load(p12stream, p12pass.toCharArray());
        p12stream.close();

        // Get Certificate & Private key from KeyStore
        PrivateKey pKey = (PrivateKey) ks.getKey(signer, p12pass.toCharArray());
        X509Certificate pCertificate = (X509Certificate)ks.getCertificate(signer);
        String SignerDN = pCertificate.getSubjectDN().toString();

        // Create a list of Certificates to include in the final CMS
        ArrayList<X509Certificate> certList = new ArrayList<X509Certificate>();
        certList.add(pCertificate);

        if (Security.getProvider("BC") == null) {
            Security.addProvider(new BouncyCastleProvider());
        }

        CertStore cstore = CertStore.getInstance("Collection", new CollectionCertStoreParameters(certList), "BC");


        //
        // Create XML Message
        //
        String LoginTicketRequest_xml = createLoginTicketRequest(SignerDN, dstDN, service, TicketTime);


        //
        // Create CMS Message
        //

        // Create a new empty CMS Message
        CMSSignedDataGenerator gen = new CMSSignedDataGenerator();

        // Add a Signer to the Message
        gen.addSigner(pKey, pCertificate, CMSSignedDataGenerator.DIGEST_SHA1);

        // Add the Certificate to the Message
        gen.addCertificatesAndCRLs(cstore);

        // Add the data (XML) to the Message
        CMSProcessable data = new CMSProcessableByteArray(LoginTicketRequest_xml.getBytes());

        // Add a Sign of the Data to the Message
        CMSSignedData signed = gen.generate(data, true, "BC");


        return (signed.getEncoded());

    }

    //
    // Create XML Message for AFIP wsaa
    //
    private String createLoginTicketRequest(String SignerDN, String dstDN, String service, Long TicketTime) {

        Date GenTime = new Date();
        GregorianCalendar gentime = new GregorianCalendar();
        GregorianCalendar exptime = new GregorianCalendar();
        String UniqueId = Long.toString(GenTime.getTime() / 1000);

        exptime.setTime(new Date(GenTime.getTime() + TicketTime));

        XMLGregorianCalendarImpl XMLGenTime = new XMLGregorianCalendarImpl(gentime);
        XMLGregorianCalendarImpl XMLExpTime = new XMLGregorianCalendarImpl(exptime);

		return "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                +"<loginTicketRequest version=\"1.0\">"
                +"<header>"
                +"<source>" + SignerDN + "</source>"
                +"<destination>" + dstDN + "</destination>"
                +"<uniqueId>" + UniqueId + "</uniqueId>"
                +"<generationTime>" + XMLGenTime + "</generationTime>"
                +"<expirationTime>" + XMLExpTime + "</expirationTime>"
                +"</header>"
                +"<service>" + service + "</service>"
                +"</loginTicketRequest>";

    }

    public WSLoginData solicitarAuth(WSConfig config, String service) throws Exception {

        // Invoke AFIP wsaa and get LoginTicketResponse

        // Create LoginTicketRequest_xml_cms
        byte [] LoginTicketRequestXmlCms = createCms(config.getp12file(), config.getP12pass(), config.getSigner(),
                config.getDstDN(), service, config.getTicketTime());

        System.out.println("Invoiking WSAA: " + config.getEndpoint());

		String LoginTicketResponse = invokeWsaa(LoginTicketRequestXmlCms, config.getEndpoint());

        Reader tokenReader = new StringReader(LoginTicketResponse);
        Document tokenDoc = new SAXReader(false).read(tokenReader);

        String token = tokenDoc.valueOf("/loginTicketResponse/credentials/token");
        String sign = tokenDoc.valueOf("/loginTicketResponse/credentials/sign");

        return new WSLoginData(token, sign, LocalDateTime.now().plusSeconds(config.getTicketTime()), service);

    }

}

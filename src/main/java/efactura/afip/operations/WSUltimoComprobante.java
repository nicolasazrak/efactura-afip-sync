package efactura.afip.operations;

import com.google.inject.Inject;
import efactura.afip.utils.Reference;
import efactura.afip.utils.xmlStringParser;
import nu.xom.*;
import org.dom4j.DocumentException;

import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import java.io.IOException;
import java.net.URL;

public class WSUltimoComprobante {

	private final SOAPConnection connection;

	@Inject
	private WSUltimoComprobante(SOAPConnectionFactory connectionFactory) throws SOAPException {
		this.connection = connectionFactory.createConnection();
	}


    public long consultar(String token, String sign, long cuit, int ptoVta, int cbteTipo) throws SOAPException, IOException, DocumentException, ParsingException {

		String xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
                "  <soap:Body>\n" +
                "    <FECompUltimoAutorizado xmlns=\"http://ar.gov.afip.dif.FEV1/\">\n" +
                "      <Auth>\n" +
                "        <Token>"+token +"</Token>\n" +
                "        <Sign>"+sign+"</Sign>\n" +
                "        <Cuit>"+cuit+"</Cuit>\n" +
                "      </Auth>\n" +
                "      <PtoVta>"+ptoVta+"</PtoVta>\n" +
                "      <CbteTipo>"+cbteTipo+"</CbteTipo>\n" +
                "    </FECompUltimoAutorizado>\n" +
                "  </soap:Body>\n" +
                "</soap:Envelope>";

        SOAPMessage message = xmlStringParser.getSoapMessageFromString(xml);
        message.getMimeHeaders().addHeader("SOAPAction", "http://ar.gov.afip.dif.FEV1/FECompUltimoAutorizado");
        SOAPMessage response = connection.call(message, new URL(Reference.wsUltComp));
        String responseString = xmlStringParser.getStringFromSoapMessage(response);
        System.out.println(responseString);

        Document document = new Builder().build(responseString, "test.xml");
        Element rootElem = document.getRootElement();
        XPathContext xc = XPathContext.makeNamespaceContext(rootElem);
        xc.addNamespace("fev1", "http://ar.gov.afip.dif.FEV1/");
        Nodes matchedNodes = rootElem.query("/soap:Envelope/soap:Body/fev1:FECompUltimoAutorizadoResponse/fev1:FECompUltimoAutorizadoResult/fev1:CbteNro", xc);
        return Long.parseLong(matchedNodes.get(0).getValue());

    }

}

import com.google.inject.Guice;
import com.google.inject.Injector;
import efactura.afip.WSCommon.WSConfig;
import efactura.afip.WSCommon.WSLoginData;
import efactura.afip.models.Factura.Cabecera;
import efactura.afip.models.Factura.Factura;
import efactura.afip.models.Factura.Moneda;
import efactura.afip.operations.AfipWebServicesBindings;
import efactura.afip.operations.WSSolicitarCAE;
import efactura.afip.operations.WSUltimoComprobante;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;

public class TestGetCae {

	WSSolicitarCAE service;
	WSLoginData loginData;
	WSUltimoComprobante ultimoComprobante;

	@Before
	public void getService() throws Exception {
		Injector injector = Guice.createInjector(new AfipWebServicesBindings());

		/*Injector injector = Guice.createInjector(new AbstractModule() {
			@Override
			protected void configure() {}

			@Provides
			protected SOAPConnectionFactory getConnection() throws SOAPException, IOException {

				SOAPConnectionFactory factory = mock(SOAPConnectionFactory.class);

				SOAPConnection connection = mock(SOAPConnection.class);

				when(factory.createConnection()).thenReturn(connection);
				when(connection.call(any(SOAPMessage.class), any(URL.class))).thenReturn(new Message(new Object(), false) {
					@Override
					public void removeAttachments(MimeHeaders mimeHeaders) {}

					@Override
					public AttachmentPart getAttachment(SOAPElement soapElement) throws SOAPException {	return null; }

					@Override
					public void writeTo(OutputStream outputStream) throws SOAPException, IOException {

						byte[] facturaRepetida = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"><soap:Body><FECAESolicitarResponse xmlns=\"http://ar.gov.afip.dif.FEV1/\"><FECAESolicitarResult><FeCabResp><Cuit>20380296056</Cuit><PtoVta>12</PtoVta><CbteTipo>1</CbteTipo><FchProceso>20150811161024</FchProceso><CantReg>1</CantReg><Resultado>R</Resultado><Reproceso>N</Reproceso></FeCabResp><FeDetResp><FECAEDetResponse><Concepto>1</Concepto><DocTipo>80</DocTipo><DocNro>20111111112</DocNro><CbteDesde>456456</CbteDesde><CbteHasta>456456</CbteHasta><CbteFch>20150807</CbteFch><Resultado>R</Resultado><CAE /><CAEFchVto /></FECAEDetResponse></FeDetResp><Errors><Err><Code>10016</Code><Msg>El nÃºmero o fecha del comprobante no se corresponde con el prÃ³ximo a autorizar. Consultar metodo FECompUltimoAutorizado.</Msg></Err></Errors></FECAESolicitarResult></FECAESolicitarResponse></soap:Body></soap:Envelope>\n".getBytes();
						btye[] cuitInvalido = "<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><FECAESolicitarResponse xmlns="http://ar.gov.afip.dif.FEV1/"><FECAESolicitarResult><Errors><Err><Code>600</Code><Msg>ValidacionDeToken: No apareció CUIT en lista de relaciones: 20280296056</Msg></Err></Errors></FECAESolicitarResult></FECAESolicitarResponse></soap:Body></soap:Envelope>".getBytes();
						byte[] fechaDesdeInvalida = "<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><FECAESolicitarResponse xmlns="http://ar.gov.afip.dif.FEV1/"><FECAESolicitarResult><FeCabResp><Cuit>20380296056</Cuit><PtoVta>12</PtoVta><CbteTipo>1</CbteTipo><FchProceso>20150816173501</FchProceso><CantReg>1</CantReg><Resultado>R</Resultado><Reproceso>N</Reproceso></FeCabResp><FeDetResp><FECAEDetResponse><Concepto>1</Concepto><DocTipo>80</DocTipo><DocNro>10</DocNro><CbteDesde>0</CbteDesde><CbteHasta>0</CbteHasta><CbteFch>20150816</CbteFch><Resultado>R</Resultado><CAE /><CAEFchVto /></FECAEDetResponse></FeDetResp><Errors><Err><Code>10008</Code><Msg>Campo CbteDesde se encuentre entre  entre 1 y 99999999.</Msg></Err></Errors></FECAESolicitarResult></FECAESolicitarResponse></soap:Body></soap:Envelope>".getBytes();

						outputStream.write(facturaRepetida);
					}

				});

				return factory;

			}

		});*/

		ultimoComprobante = injector.getInstance(WSUltimoComprobante.class);
		service = injector.getInstance(WSSolicitarCAE.class);
		loginData = new WSLoginData(
				"PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/Pgo8c3NvIHZlcnNpb249IjIuMCI+CiAgICA8aWQgdW5pcXVlX2lkPSIxNTQ5NTgwOTA1IiBzcmM9IkNOPXdzYWFob21vLCBPPUFGSVAsIEM9QVIsIFNFUklBTE5VTUJFUj1DVUlUIDMzNjkzNDUwMjM5IiBnZW5fdGltZT0iMTQzOTc1MzUyMiIgZXhwX3RpbWU9IjE0Mzk3OTY3ODIiIGRzdD0iQ049d3NmZSwgTz1BRklQLCBDPUFSIi8+CiAgICA8b3BlcmF0aW9uIHZhbHVlPSJncmFudGVkIiB0eXBlPSJsb2dpbiI+CiAgICAgICAgPGxvZ2luIHVpZD0iQz1hciwgTz1uaWNvbGFzIGF6cmFrLCBTRVJJQUxOVU1CRVI9Q1VJVCAyMDM4MDI5NjA1NiwgQ049ZWZhY3R1cmEiIHNlcnZpY2U9IndzZmUiIHJlZ21ldGhvZD0iMjIiIGVudGl0eT0iMzM2OTM0NTAyMzkiIGF1dGhtZXRob2Q9ImNtcyI+CiAgICAgICAgICAgIDxyZWxhdGlvbnM+CiAgICAgICAgICAgICAgICA8cmVsYXRpb24gcmVsdHlwZT0iNCIga2V5PSIyMDM4MDI5NjA1NiIvPgogICAgICAgICAgICA8L3JlbGF0aW9ucz4KICAgICAgICA8L2xvZ2luPgogICAgPC9vcGVyYXRpb24+Cjwvc3NvPgoK",
				"oS28Cl7c2EwIVqfBn++Ixv+ms/DEDyJb0wb0pazR/5+eYJ9Nryaz6+I8Qxe+EjuV49VlSSinYVCHNscvsZPCyXK6pEdV8jaSmYBko1OVx8QaQKML4f4S5jk3SemPnvSR7QaWaz8/gXMRWm0as2gAeWWV3BatIDFV6hUhuup8Wvg=",
				LocalDateTime.now(),
				"wsfe"
		);

	}

	@Test
	public void testGetCae(){

		try {

			Long numeroComprobante = ultimoComprobante.consultar(loginData.getToken(), loginData.getSign(), 20380296056l, 12, 1);

			System.out.println("El ultimo numero es: " + numeroComprobante);

			Cabecera cabecera = new Cabecera().setCuit(20380296056l).setFecha("20150816").setPuntoDeVenta(12)
																	.setTipo(1).setConcepto(1).setDocumentoTipo(80)
																	.setDesde(456456).setHasta(456456)
																	.setNumero(10);

			Factura factura = new Factura(cabecera, 1, 184.05, 0, 150, 0, 7.8, 26.25, "", "", "", Moneda.Pesos());
			factura.agregarTributo((short) 99, "Impuesto Municipal Matanza", 150, 5.2, 7.8);
			factura.agregarIva((short) 5, 100, 21);
			factura.agregarIva((short) 4, 50, 5.25);

			String cae = service.solicitar(new WSConfig(), loginData, factura);

			Assert.assertNotNull(cae);

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}

	}


}

import efactura.afip.WSCommon.WSError;
import efactura.afip.WSCommon.WSEvent;
import efactura.afip.WSRespuestaDeComprobante.*;
import org.apache.commons.io.FileUtils;
import org.junit.Test;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class WsRespuestaTest {

    @Test
    public void seGeneraBienLaClaseDesdeUnXML() throws JAXBException, IOException, InterruptedException {

        File test2 = new File("src/main/testFiles/test2.xml");
        WSRespuesta wsRespuesta = WSRespuesta.createWsRespuesta(test2);

        FeCabResp feCabResp= new FeCabResp(123,10,15,"10/5/06",15,"sad");
        List<Observacion> observs = new ArrayList<Observacion>();
        observs.add(new Observacion(855,"dfgfgdfgdfgd fdgdfgdfg"));
        Observaciones observaciones = new Observaciones(observs);
        FeDetResponse feDetResponse = new FeDetResponse(85,9657148,549841,541981,4857,"asdasdaw","148597521","10/9/06","sgfdfgtring",observaciones);
        FeDetResp feDetResp=new FeDetResp(feDetResponse);
        List<WSEvent> evs = new ArrayList<WSEvent>();
        evs.add(new WSEvent(789156456,"dsfgdfger"));
        Events events = new Events(evs);
        List<WSError> errs = new ArrayList<WSError>();
        errs.add(new WSError(87715,"fsdgsdfgs"));
        Errors errors = new Errors(errs);
        FECAESolicitarResult fECAESolicitarResult= new FECAESolicitarResult(feCabResp, feDetResp,events,errors);
        FECAESolicitarResponse fecaeSolicitarResponse=new FECAESolicitarResponse(fECAESolicitarResult);
        Body body = new Body(fecaeSolicitarResponse);
        WSRespuesta respuestaComprobador = new WSRespuesta(body);

        JAXBContext jaxbContext = JAXBContext.newInstance(WSRespuesta.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();


        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        File comprobador = new File("src/main/testFiles/test.xml");
        jaxbMarshaller.marshal(wsRespuesta, comprobador);
        File comprobador2 = new File("src/main/testFiles/test3.xml");
        jaxbMarshaller.marshal(respuestaComprobador, comprobador2);


        assertTrue(FileUtils.contentEquals(comprobador2, comprobador));
        assertEquals(FileUtils.readLines(comprobador), FileUtils.readLines(test2));//uso otro sistema porque por algun motivo el test no da

    }

}

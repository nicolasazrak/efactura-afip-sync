import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Provides;
import efactura.afip.WSCommon.WSConfig;
import efactura.afip.WSCommon.WSLoginData;
import efactura.afip.operations.WSSolicitudTA;
import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.xml.rpc.ServiceException;
import java.rmi.RemoteException;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class WsaaTest {

	WSSolicitudTA service;

	@Before
	public void getService(){

		Injector injector = Guice.createInjector(new AbstractModule(){

			@Override
			protected void configure() {}

			@Provides
			Service provideClient() throws ServiceException, RemoteException {

				Service client = mock(Service.class);
				Call call = mock(Call.class);

				when(client.createCall()).thenReturn(call);
				when(call.invoke(any(Object[].class))).thenReturn("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
						"<loginTicketResponse version=\"1\">\n" +
						"    <header>\n" +
						"        <source>CN=wsaahomo, O=AFIP, C=AR, SERIALNUMBER=CUIT 33693450239</source>\n" +
						"        <destination>C=ar, O=nicolas azrak, SERIALNUMBER=CUIT 20280296056, CN=efactura</destination>\n" +
						"        <uniqueId>2071748170</uniqueId>\n" +
						"        <generationTime>2015-08-16T16:33:02.547-03:00</generationTime>\n" +
						"        <expirationTime>2015-08-17T04:33:02.547-03:00</expirationTime>\n" +
						"    </header>\n" +
						"    <credentials>\n" +
						"        <token>PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/Pgo8c3NvIHZlcnNpb249IjIuMCI+CiAgICA8aWQgdW5pcXVlX2lkPSIxNTQ5NTgwOTA1IiBzcmM9IkNOPXdzYWFob21vLCBPPUFGSVAsIEM9QVIsIFNFUklBTE5VTUJFUj1DVUlUIDMzNjkzNDUwMjM5IiBnZW5fdGltZT0iMTQzOTc1MzUyMiIgZXhwX3RpbWU9IjE0Mzk3OTY3ODIiIGRzdD0iQ049d3NmZSwgTz1BRklQLCBDPUFSIi8+CiAgICA8b3BlcmF0aW9uIHZhbHVlPSJncmFudGVkIiB0eXBlPSJsb2dpbiI+CiAgICAgICAgPGxvZ2luIHVpZD0iQz1hciwgTz1uaWNvbGFzIGF6cmFrLCBTRVJJQUxOVU1CRVI9Q1VJVCAyMDM4MDI5NjA1NiwgQ049ZWZhY3R1cmEiIHNlcnZpY2U9IndzZmUiIHJlZ21ldGhvZD0iMjIiIGVudGl0eT0iMzM2OTM0NTAyMzkiIGF1dGhtZXRob2Q9ImNtcyI+CiAgICAgICAgICAgIDxyZWxhdGlvbnM+CiAgICAgICAgICAgICAgICA8cmVsYXRpb24gcmVsdHlwZT0iNCIga2V5PSIyMDM4MDI5NjA1NiIvPgogICAgICAgICAgICA8L3JlbGF0aW9ucz4KICAgICAgICA8L2xvZ2luPgogICAgPC9vcGVyYXRpb24+Cjwvc3NvPgoK</token>\n" +
						"        <sign>oS28Cl7c2EwIVqfBn++Ixv+ms/DEDyJb0wb0pazR/5+eYJ9Nryaz6+I8Qxe+EjuV49VlSSinYVCHNscvsZPCyXK6pEdV8jaSmYBko1OVx8QaQKML4f4S5jk3SemPnvSR7QaWaz8/gXMRWm0as2gAeWWV3BatIDFV6hUhuup8Wvg=</sign>\n" +
						"    </credentials>\n" +
						"</loginTicketResponse>");

				return client;

			}


		});

		service = injector.getInstance(WSSolicitudTA.class);

	}



	@Test
	public void testAuth() {

		try {

			WSLoginData loginData = service.solicitarAuth(new WSConfig(), "wsfe");
			Assert.assertEquals(loginData.getToken(), "PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/Pgo8c3NvIHZlcnNpb249IjIuMCI+CiAgICA8aWQgdW5pcXVlX2lkPSIxNTQ5NTgwOTA1IiBzcmM9IkNOPXdzYWFob21vLCBPPUFGSVAsIEM9QVIsIFNFUklBTE5VTUJFUj1DVUlUIDMzNjkzNDUwMjM5IiBnZW5fdGltZT0iMTQzOTc1MzUyMiIgZXhwX3RpbWU9IjE0Mzk3OTY3ODIiIGRzdD0iQ049d3NmZSwgTz1BRklQLCBDPUFSIi8+CiAgICA8b3BlcmF0aW9uIHZhbHVlPSJncmFudGVkIiB0eXBlPSJsb2dpbiI+CiAgICAgICAgPGxvZ2luIHVpZD0iQz1hciwgTz1uaWNvbGFzIGF6cmFrLCBTRVJJQUxOVU1CRVI9Q1VJVCAyMDM4MDI5NjA1NiwgQ049ZWZhY3R1cmEiIHNlcnZpY2U9IndzZmUiIHJlZ21ldGhvZD0iMjIiIGVudGl0eT0iMzM2OTM0NTAyMzkiIGF1dGhtZXRob2Q9ImNtcyI+CiAgICAgICAgICAgIDxyZWxhdGlvbnM+CiAgICAgICAgICAgICAgICA8cmVsYXRpb24gcmVsdHlwZT0iNCIga2V5PSIyMDM4MDI5NjA1NiIvPgogICAgICAgICAgICA8L3JlbGF0aW9ucz4KICAgICAgICA8L2xvZ2luPgogICAgPC9vcGVyYXRpb24+Cjwvc3NvPgoK");
			Assert.assertEquals(loginData.getSign(), "oS28Cl7c2EwIVqfBn++Ixv+ms/DEDyJb0wb0pazR/5+eYJ9Nryaz6+I8Qxe+EjuV49VlSSinYVCHNscvsZPCyXK6pEdV8jaSmYBko1OVx8QaQKML4f4S5jk3SemPnvSR7QaWaz8/gXMRWm0as2gAeWWV3BatIDFV6hUhuup8Wvg=");

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Throw exception");
		}

	}
}
